package com.ck.concurrency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConcurrencyApplication {
	public static void main(String[] args) {
		// 2019年7月6日16:45:32
		SpringApplication.run(ConcurrencyApplication.class, args);
	}
}
